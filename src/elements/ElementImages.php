<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use Hestec\ElementalExtensions\Dataobjects\ImageObject;
use SilverStripe\Forms\NumericField;

class ElementImages extends BaseElement
{

    private static $table_name = 'HestecElementImages';

    private static $singular_name = 'ElementImage';

    private static $plural_name = 'ElementImages';

    private static $description = 'Element with images horizontal';

    private static $icon = 'font-icon-p-gallery';

    private static $db = [
        'Content' => 'HTMLText',
        'ContentBelow' => 'HTMLText',
        'Style' => "Enum('NoColsLeft,NoColsCenter,InColsLeft,InColsCenter','NoColsLeft')",
        'NumberOfImages' => 'Int'
    ];

    private static $defaults = array(
        'NumberOfImages' => 4
    );

    private static $has_many = array(
        'ImageObjects' => ImageObject::class
    );

    private static $inline_editable = false;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('ImageObjects');

        $StyleSource = array(
            'NoColsLeft' => _t('ElementImages.NOCOLSLEFT', "not in columns, left-aligned"),
            'NoColsCenter' => _t('ElementImages.NOCOLSCENTER', "not in columns, centered"),
            'InColsLeft' => _t('ElementImages.INCOLSLEFT', "in columns, left-aligned"),
            'InColsCenter' => _t('ElementImages.INCOLSCENTER', "in columns, centered")
        );

        $ContentField = HTMLEditorField::create('Content', "Content");
        $ContentField->setRows(5);
        $ContentBelowField = HTMLEditorField::create('ContentBelow', _t('Element.CONTENTBELOW', "Content below"));
        $ContentBelowField->setRows(5);
        $StyleField = DropdownField::create('Style', _t('ElementImages.DISPLAY_IMAGES', "Display images"), $StyleSource);
        $NumberOfImagesField = NumericField::create('NumberOfImages', _t('ElementImages.NUMBER_OF_IMAGES', "Number of images"));

        $fields->addFieldToTab('Root.Main', $ContentField);
        $fields->addFieldToTab('Root.Main', $ContentBelowField);
        $fields->addFieldToTab('Root.Main', $StyleField);
        $fields->addFieldToTab('Root.Main', $NumberOfImagesField);

        if ($this->ID) {

            $ImageObjectsGridField = GridField::create(
                'ImageObjects',
                _t('ElementImages.IMAGES', "Images"),
                $this->ImageObjects(),
                GridFieldConfig_RecordEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
            );

            $fields->addFieldToTab('Root.Main', $ImageObjectsGridField);
        }
        return $fields;
    }

    public function getType()
    {
        return 'Images';
    }
}
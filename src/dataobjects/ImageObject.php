<?php

namespace Hestec\ElementalExtensions\Dataobjects;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Assets\Image;
use SilverStripe\Security\Permission;
use Hestec\ElementalExtensions\Elements\ElementImages;

class ImageObject extends DataObject {

    private static $table_name = 'HestecElementImageObject';

    private static $singular_name = 'Image';
    private static $plural_name = 'Images';

    private static $db = [
        'Title' => 'Varchar(255)',
        'Caption' => 'Varchar(255)',
        'Sort' => 'Int'
    ];

    private static $default_sort = 'Sort';

    private static $has_one = [
        'ElementImages' => ElementImages::class,
        'Image' => Image::class
    ];

    private static $owns = [
        'Image'
    ];

    private static $summary_fields = [
        'getThumbnail',
        'Title'
    ];

    function getThumbnail()
    {
        if ($Image = $this->Image())
        {
            return $Image->CMSThumbnail();
        }
        else
        {
            return '(No Image)';
        }
    }

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['getThumbnail'] = _t("ElementImages.IMAGE", "Image");
        $labels['Title'] = _t("Element.TITLE", "Title");

        return $labels;
    }

    public function getCMSFields()
    {
        $TitleField = TextField::create('Title', _t("Element.TITLE", "Title"));
        $ImageField = UploadField::create('Image', _t("ElementImages.IMAGE", "Image"));
        $CaptionField = TextField::create('Caption', _t("Element.CAPTION", "Caption"));

        return new FieldList(
            $TitleField,
            $ImageField,
            $CaptionField
        );

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}
